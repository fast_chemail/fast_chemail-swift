// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// https://tools.ietf.org/html/rfc5321#section-4.5.3.1
//
// The maximum total length of a user name or other local-part is 64 octets.
// The maximum total length of a domain name or number is 255 octets.
let maxLocalPart = 64
let maxDomainPart = 255
let maxLabel = 63

// `isValidEmail` checks wheter an email address is valid.
public func isValidEmail(address: String) -> Bool {
    do {
        try parseEmail(address: address)
    } catch {
        return false
    }
    return true
}

// `parseEmail` scans an email address to check wheter it is correct.
public func parseEmail(address: String) throws {
    if address[address.startIndex] == "@" {
        throw ParseError.noLocalPart
    }
    if address[address.endIndex] == "@" {
        throw ParseError.noDomainPart
    }

	// https://tools.ietf.org/html/rfc5321#section-4.1.2
	//
	// Systems MUST NOT define mailboxes in such a way as to require the use in
	// SMTP of non-ASCII characters (octets with the high order bit set to one)
	// or ASCII "control characters" (decimal value 0-31 and 127).
    try checkASCIIPrintable(str: address)

    let parts = address.split{$0 == "@"}.map(String.init)
    if parts.count != 2 {
        if parts.count > 2 {
            throw ParseError.tooAt
        }
        throw ParseError.noSignAt
    }

    // == Local part

    // https://tools.ietf.org/html/rfc3696#section-3
    //
    // Period (".") may also appear, but may not be used to start or end
    // the local part, nor may two or more consecutive periods appear.

    if parts[0].count > maxLocalPart {
        throw ParseError.localTooLong
    }
    if parts[0][parts[0].startIndex] == "." {
        throw ParseError.localStartPeriod
    }
    if parts[0][parts[0].endIndex] == "." {
        throw ParseError.localEndPeriod
    }

    var lastPeriod = false
    for char in parts[0] {
        if isLetter(char: char) || isDigit(char: char) {
            if lastPeriod {
                lastPeriod = false
            }
            continue
        }

        switch char {
        case ".":
            if lastPeriod {
                throw ParseError.consecutivePeriod
            }
        case "!", "#", "$", "%", "&", "'", "*", "+", "-", "/", "=", "?", "^",
            "_", "`", "{", "|", "}", "~":
            // atom :: https://tools.ietf.org/html/rfc5322#section-3.2.3
            if lastPeriod {
                lastPeriod = false
            }
        default:
            throw ParseError.wrongCharLocal(char: char)
        }
    }

	// == Domain part

	// https://tools.ietf.org/html/rfc5321#section-4.1.2
	//
	// characters outside the set of alphabetic characters, digits, and hyphen
	// MUST NOT appear in domain name labels for SMTP clients or servers.  In
	// particular, the underscore character is not permitted.

	// https://tools.ietf.org/html/rfc1034#section-3.5
	//
	// The labels must follow the rules for ARPANET host names.  They must start
	// with a letter, end with a letter or digit, and have as interior
	// characters only letters, digits, and hyphen.  There are also some
	// restrictions on the length.  Labels must be 63 characters or less.

	// https://tools.ietf.org/html/rfc3696#section-2
	//
	// It is likely that the better strategy has now become to make the "at
	// least one period" test, to verify LDH conformance (including verification
	// that the apparent TLD name is not all-numeric), and then to use the DNS
	// to determine domain name validity, rather than trying to maintain a local
	// list of valid TLD names.

    if parts[1].count > maxDomainPart {
        throw ParseError.domainTooLong
    }
    if parts[1][parts[1].startIndex] == "." {
        throw ParseError.domainStartPeriod
    }
    if parts[1][parts[1].endIndex] == "." {
        throw ParseError.domainEndPeriod
    }

    let labels = parts[1].split{$0 == "."}.map(String.init)
    if labels.count == 1 {
        throw ParseError.noPeriodDomain
    }

    // label = let-dig [ [ ldh-str ] let-dig ]
	// limited to a length of 63 characters by RFC 1034 section 3.5
	//
	// <let-dig> ::= <letter> | <digit>
	// <ldh-str> ::= <let-dig-hyp> | <let-dig-hyp> <ldh-str>
	// <let-dig-hyp> ::= <let-dig> | "-"
    for label in labels {
        if label.isEmpty {
            throw ParseError.consecutivePeriod
        }
        if label.count > maxLabel {
            throw ParseError.labelTooLong
        }

        for char in label {
            if isLetter(char: char) || isDigit(char: char) || char == "-" {
                continue
            }
            throw ParseError.wrongCharDomain(char: char)
        }

        let firstChar = label[label.startIndex]
        if !isLetter(char: firstChar) && !isDigit(char: firstChar) {
            throw ParseError.wrongStartLabel(char: label[label.startIndex])
        }
        let lastChar = label[label.endIndex]
        if !isLetter(char: lastChar) && !isDigit(char: lastChar) {
            throw ParseError.wrongEndLabel(char: lastChar)
        }
    }
}

// == Errors
//

public enum ParseError: Error {
    case noLocalPart
    case noDomainPart
    case noSignAt

    case tooAt
    case localTooLong
    case domainTooLong
    case labelTooLong

    case localStartPeriod
    case localEndPeriod
    case domainStartPeriod
    case domainEndPeriod
    case consecutivePeriod
    case noPeriodDomain

    case ascii(err: AsciiError)
    case wrongCharLocal(char: Character)
    case wrongCharDomain(char: Character)
    case wrongStartLabel(char: Character)
    case wrongEndLabel(char: Character)

    var message: String {
        switch self {
        case .noLocalPart:
            return "no local part"
        case .noDomainPart:
            return "no domain part"
        case .noSignAt:
            return "no at sign (@)"

        case .tooAt:
            return "wrong number of at sign (@)"
        case .localTooLong:
            return "the local part has more than \(maxLocalPart) characters"
        case .domainTooLong:
            return "the domain part has more than \(maxDomainPart) characters"
        case .labelTooLong:
            return "a domain label has more than \(maxLabel) characters"

        case .localStartPeriod:
            return "the local part starts with a period"
        case .localEndPeriod:
            return "the local part ends with a period"
        case .domainStartPeriod:
            return "the domain part starts with a period"
        case .domainEndPeriod:
            return "the domain part ends with a period"
        case .consecutivePeriod:
            return "appear two or more consecutive periods"
        case .noPeriodDomain:
            return "no period at domain part"

        case .ascii(let err):
            return "\(err)"
        case .wrongCharLocal(let char):
            return "character not valid in local part (\(char))"
        case .wrongCharDomain(let char):
            return "character not valid in domain part (\(char))"
        case .wrongStartLabel(let char):
            return "character not valid at start of domain label (\(char))"
        case .wrongEndLabel(let char):
            return "character not valid at end of domain label (\(char))"
        }
    }
}
