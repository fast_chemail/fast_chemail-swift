// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

let maxAscii: UInt8 = 127

func isLetter(char: Character) -> Bool {
    if (char >= "a" && char <= "z") || (char >= "A" && char <= "Z") {
        return true
    }
    return false
}

func isDigit(char: Character) -> Bool {
    return "0" <= char && char <= "9"
}

// `checkASCIIPrintable` reports an error wheter the string has a non-ASCII
// character or any ASCII control character.
func checkASCIIPrintable(str: String) throws {
    var idx = 0
    for code in str.utf8 {
        if code > maxAscii {
            let index = str.index(str.startIndex, offsetBy: idx)
            throw AsciiError.nonAscii(char: str[index])
        }
        if 0 <= code && code <= 31 || code == 127 {
            throw AsciiError.controlChar(position: idx+1)
        }
        idx += 1
    }
}

public enum AsciiError: Error {
    case nonAscii(char: Character)
    case controlChar(position: Int)

    var message: String {
        switch self {
        case .nonAscii(let char):
            return "contain non US-ASCII character (\(char))"
        case .controlChar(let pos):
            return "contain ASCII control character at position \(pos)"
        }
    }
}
