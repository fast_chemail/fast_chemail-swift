// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import XCTest
@testable import FastChemail

let tableOk: [String] = [
	"!#$%&'*+-/=?^_`{|}~@example.com",
	"user+mailbox@example.com",
	"customer/department=shipping@example.com",
	"$A12345@example.com",
	"!def!xyz%abc@example.com",
	"_somename@example.com",
	"a@example.com",
	"a@x.y",
	"abc.def@example.com",
	"abc-def@example.com",
	"123@example.com",
	"xn--abc@example.com",
	"abc@x.y.z",
	"abc@xyz-example.com",
	"abc@c--n.com",
	"abc@xn--hxajbheg2az3al.xn--jxalpdlp",
	"abc@1example.com",
	"abc@x.123",
]

let tableError: [(String, Error)] = [
	("@", ParseError.noLocalPart),
	("@example.com", ParseError.noLocalPart),
	("abc@", ParseError.noDomainPart),
	("abc", ParseError.noSignAt),
	("abc@def@example.com", ParseError.tooAt),
	(".abc@example.com", ParseError.localStartPeriod),
	("abc.@example.com", ParseError.localEndPeriod),
	("abc@.example.com", ParseError.domainStartPeriod),
	("abc@example.com.", ParseError.domainEndPeriod),
	("ab..cd@example.com", ParseError.consecutivePeriod),
	("abc@example..com", ParseError.consecutivePeriod),
	("a@example", ParseError.noPeriodDomain),
	("ab\\c@example.com", ParseError.wrongCharLocal(char: "\\")),
	("ab\"c\"def@example.com", ParseError.wrongCharLocal(char: "\"")),
	("abc def@example.com", ParseError.wrongCharLocal(char: " ")),
	("(comment)abc@example.com", ParseError.wrongCharLocal(char: "(")),
	("abc@[255.255.255.255]", ParseError.wrongCharDomain(char: "[")),
	("abc@(example.com", ParseError.wrongCharDomain(char: "(")),
	("abc@x.y_y.z", ParseError.wrongCharDomain(char: "_")),
	("abc@-example.com", ParseError.wrongStartLabel(char: "-")),
	("abc@example-.com", ParseError.wrongEndLabel(char: "-")),
	("abc@x.-y.z", ParseError.wrongStartLabel(char: "-")),
	("abc@x.y-.z", ParseError.wrongEndLabel(char: "-")),
	/*("abcd€f@example.com", ParseError.ascii(err: AsciiError.nonAscii(char: "€"))),
	("abc@exámple.com", ParseError.ascii(err: AsciiError.nonAscii(char: "á")),
	("a\tbc@example.com", ParseError.ascii(err: AsciiError.controlChar(position: 2)),
	("abc@\texample.com", ParseError.ascii(err: AsciiError.controlChar(position: 5)),*/
]

class ParserTestCase: XCTestCase {
    static var allTests = {
        return [
            //("test_parseEmail", test_parseEmail),
            //("test_isValidEmail", test_isValidEmail),
            ("test_length", test_length),
        ]
    }()

    func test_parseEmail() {
		for (index, value) in tableOk.enumerated() {
			do {
				try parseEmail(address: value)
			} catch {
				XCTFail("[\(index)] `\(value)` got error")
			}
		}

		/*for (index, value) in tableError.enumerated() {
		}*/
    }

    func test_isValidEmail() {
		if !isValidEmail(address: tableOk[0]) {
			XCTFail("[0] `\(tableOk[0])` want `true`")
		}

		if isValidEmail(address: tableError[0].0) {
			XCTFail("[0] `\(tableError[0].0)` want `false`")
		}
    }

    func test_length() {
		let localPart = String(repeating: "a", count: maxLocalPart)

		let label = String(repeating: "x", count: maxLabel)// + "."
		let allLabels = String(repeating: label, count: 3)
		let lastLabel = String(repeating: "y", count: maxDomainPart-allLabels.count)

		let inputOk = "\(localPart)@\(allLabels)\(lastLabel)"
		do {
			try parseEmail(address: inputOk)
		} catch {
			XCTFail("[\(inputOk)] got error")
		}

		// == Errors

    }
}
