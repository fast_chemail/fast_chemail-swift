// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import XCTest
@testable import FastChemail

class AsciiTestCase: XCTestCase {
    static var allTests = {
        return [
            ("test_isLetter", test_isLetter),
            ("test_isDigit", test_isDigit),
            ("test_checkASCIIPrintable", test_checkASCIIPrintable),
        ]
    }()

    func test_isLetter() {
        XCTAssertTrue(isLetter(char: "a"), "a")
        XCTAssertTrue(isLetter(char: "j"), "j")
        XCTAssertTrue(isLetter(char: "z"), "z")
        XCTAssertTrue(isLetter(char: "A"), "A")
        XCTAssertTrue(isLetter(char: "J"), "J")
        XCTAssertTrue(isLetter(char: "Z"), "Z")

        XCTAssertFalse(isLetter(char: "0"), "0")
    }

    func test_isDigit() {
        XCTAssertTrue(isDigit(char: "0"), "0")
        XCTAssertTrue(isDigit(char: "5"), "5")
        XCTAssertTrue(isDigit(char: "9"), "9")

        XCTAssertFalse(isDigit(char: "a"), "a")
    }

    func test_checkASCIIPrintable() {
        let inputOk = "foo"
        do {
            try checkASCIIPrintable(str: inputOk)
        } catch {
            XCTFail("`\(inputOk)` got error")
        }

        var inputErr = "foó"
        XCTAssertThrowsError(try checkASCIIPrintable(str: inputErr)) { error in
            guard let err = error as? AsciiError else {
                XCTFail("`\(inputErr)` want error `AsciiError`")
                return
            }
            switch err {
            case .nonAscii(let char):
                XCTAssertEqual(char, "ó")
            default:
                XCTFail("`\(inputErr)` want error `AsciiError.nonAscii`")
            }
        }

        inputErr = "foo\tbar"
        XCTAssertThrowsError(try checkASCIIPrintable(str: inputErr)) { error in
            guard let err = error as? AsciiError else {
                XCTFail("`\(inputErr)` want error `AsciiError`")
                return
            }
            switch err {
            case .controlChar(let pos):
                XCTAssertEqual(pos, 4)
            default:
                XCTFail("`\(inputErr)` want error `AsciiError.controlChar`")
            }
        }
    }
}
